package Eje5;
// Autor : Jurgen Quintanilla (JP)
import java.util.Scanner;
public class Multi {
    public static void main(String[] args) {
       
        Scanner sc = new Scanner(System.in);
        int [][] MatrizA; 
        int [][] MatrizB;
        int [][] MatrizC;
        System.out.println("Multiplicación de matrices");
        // ingreso de los datos de la matriz A
        System.out.println("Ingrese las dimensiones de la Matriz A");
        System.out.println("Numero de filas: "); int f1 = sc.nextInt();
        System.out.println("Numero de columnas: "); int c1 = sc.nextInt();
        // ingreso de los datos de la matriz B
        System.out.println("Ingrese las dimensiones de la Matriz B");
        System.out.println("Numero de filas: "); int f2 = sc.nextInt();
        System.out.println("Numero de columnas: "); int c2 = sc.nextInt();
        // condicion de la multiplicacion de matries
        MatrizA = new int [f1][c1];
        MatrizB = new int [f2][c2];
        MatrizC = new int [f1][c2];
        if (c1 == f2)
        {
            System.out.println("Las matrices A y B pueden ser multiplicadas");
            // ingreso de datos de la matriz A
            
      System.out.println("Ingresar valores a la matrizA: ");
            for (int i = 0; i < MatrizA.length; i++){
                for (int j = 0; j < MatrizA[0].length; j++){
                    System.out.print("M["+i+"]["+j+"]: ");
                    MatrizA[i][j] = sc.nextInt();
                }
            }
              // ingreso de datos de la matriz B
              
            System.out.println("Ingresar valores a la matriz B: ");
              for (int i = 0; i < MatrizB.length; i++){
                for (int j = 0; j < MatrizB[0].length; j++){
                    System.out.print("M["+i+"]["+j+"]: ");
                    MatrizB[i][j] = sc.nextInt();
                }
              }
            System.out.println("Dimensiones de la matriz resultante:"+f1+"x"+c2);

            for (int i =0; i < f1; i++)
            {
                for (int j =0; j < c2; j++)
                {
                    for (int k =0; k < c1; k ++)
                    {
                        MatrizC[i][j] += + (MatrizA[i][k]*MatrizB[k][j]);
                    }
                }
            }
            System.out.println("Matriz A:");
            dibujar(MatrizA);
            System.out.println("Matriz B:");
            dibujar(MatrizB);
            System.out.println("Matriz resultante:");
            dibujar(MatrizC);

        }
        else
        {
            System.out.println("Las matrices no se pueden multiplicar");
        }
        System.out.println("Gracias por utilizar este programa");
    }

    public static void dibujar(int [][] matriz) {
        for (int i =0; i <matriz.length; i ++)
        {
            for (int j = 0; j < matriz[0].length; j++)
            {
                System.out.print("["+matriz[i][j]+"]");
            }
            System.out.println();
        }  
         
        
    }
}
