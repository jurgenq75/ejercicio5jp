package Eje5;
// Autor: Jurgen Quintanilla // 1M3-IS
import java.util.Scanner;
public class Traza {
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[][] Matriz; 
        int sum=0;
        System.out.println("Traza de una matriz");
        System.out.println("Ingres las dimensiones de la matriz: "); int n = sc.nextInt();
        Matriz = new int [n][n]; 
        System.out.println("Ingrese los valores de la matriz "+n+"x"+n);
        for (int i = 0; i < Matriz.length; i++){
            for (int j = 0; j < Matriz.length; j++){
                System.out.print("M["+i+"]["+j+"]: ");
                Matriz[i][j] = sc.nextInt();
            }
        }
        dibujar(Matriz);
        for (int i = 0; i < Matriz.length; i++){
            for (int j = 0; j < Matriz.length; j++){
               if (i == j)
               {
                sum += Matriz [i][j];
               }
            }
        }
        System.out.println("La traza de su matriz es : "+sum);
    }
    // mostrar matriz
    public static void dibujar(int[][] matriz) {
        
        System.out.println("su matriz es: ");
        for (int i =0; i <matriz.length; i ++)
        {
            for (int j = 0; j < matriz[0].length; j++)
            {
                System.out.print("["+matriz[i][j]+"]");
            }
            System.out.println();
        }  
         
     }
}
