package Eje5;
// Autor: Jurgen Quintanilla // 1M3-IS
import java.util.Scanner;
public class Suma {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int f1, f2, c1,c2;
        int [][] matrizA, matrizB, matrizC;
        System.out.println("Suma de matrices");
        System.out.println("// Ingrese las dimensiones de la matriz A: "); 
        System.out.println("numero de filas = "); f1 = sc.nextInt();
        System.out.println("numero de columnas = "); c1 = sc.nextInt();
        System.out.println("// Ingrese las dimensiones de la matriz B "); 
        System.out.println("numero de filas = "); f2 = sc.nextInt();
        System.out.println("numero de columnas= "); c2 = sc.nextInt();
        if ((f1*c1) == (f2*c2))
        {
            System.out.println("Las matries se pueden sumar");
            matrizA = new int [f1][c1]; 
            matrizB = new int [f2][c2];
            matrizC = new int [f1][c1];
            // elementos matriz A
            for (int i =0; i < matrizA.length; i++)
            {
                for (int j=0; j < matrizA [0].length ; j++)
                    {
                        System.out.print("ingrese el elemento: ["+i+"]["+j+"] de la matriz A = ");
                        matrizA[i] [j] = sc.nextInt();
                    }
            }
            // elementos matriz B
            for (int i =0; i < matrizB.length; i++)
            {
                for (int j=0; j < matrizB [0].length ; j++)
                    {
                        System.out.print("ingrese el elemento: ["+i+"]["+j+"] de la matriz B = ");
                        matrizB[i] [j] = sc.nextInt();
                    }
            }
            // matriz resultante 
            for (int i =0; i < matrizC.length; i++)
            {
                for (int j=0; j < matrizC [0].length ; j++)
                    {
                        matrizC[i] [j] = (matrizA[i][j] + matrizB[i][j]);
                    }
            }
            // mostrar matrices
            System.out.println("Matriz A:");
            dibujar(matrizA);
            System.out.println("Matriz B:");
            dibujar(matrizB);
            System.out.println("Matriz C (suma de A + B):");
            dibujar(matrizC);
           
        }
        else
        {
            System.out.println("Las matrices no se pueden sumar");
            System.out.println("Gracias por usar este programa");
        }
    }

// registrar datos de la matriz

    
// mostrar matrices
 public static void dibujar(int[][] matriz) {
    for (int i =0; i <matriz.length; i ++)
    {
        for (int j = 0; j < matriz[0].length; j++)
        {
            System.out.print("["+matriz[i][j]+"]");
        }
        System.out.println();
    }  
     
 }
}
