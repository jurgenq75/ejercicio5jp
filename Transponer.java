package Eje5;
// Autor: Jurgen Quintanilla // 1M3-IS
import java.util.Scanner;
public class Transponer {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int matriz [][];
        int f, c; // dimensiones
        System.out.println("Transposicion de matrices");
        System.out.println("Ingrese el numero de filas de la matriz: "); f = sc.nextInt();
        System.out.println("Ingrese el numero de columnas de la matriz: "); c = sc.nextInt();
        matriz = new int [f][c];
        // condicional para ver la forma de la matriz
        if (f == c)
        {
            System.out.println("su matriz es simétrica");
            // ingreso de los valores de la matriz
            for (int i =0; i <f; i++)
            {
                for (int j=0; j <c; j++)
                    {
                        System.out.print("ingrese el elemento: ["+i+"]["+j+"]= ");
                        matriz[i] [j] = sc.nextInt();
                    }
            }
            System.out.println("Su matriz es: ");
            mostrarMatriz(matriz);
            matrizSimetrica(matriz,f ,c);                     // transposicion
            System.out.println("La matriz transpuesta es: "); // matriz transpuesta
            mostrarMatriz(matriz);
        } else 
        {
            System.out.println("su matriz es asimétrica");
            // ingreso de los valores de la matriz
                          for (int i =0; i <f; i++)
                          {
                              for (int j=0; j <c; j++)
                                  {
                                      System.out.print("ingrese el elemento: ["+i+"]["+j+"]= ");
                                      matriz[i] [j] = sc.nextInt();
                                  }
                          }
            System.out.println("Su matriz es: ");
            mostrarMatriz(matriz);     
            matrizAsimetrica(matriz);
            matriz = matrizAsimetrica(matriz);      
            System.out.println("La matriz transpuesta es: "); // matriz transpuesta
            mostrarMatriz(matriz); 
        }
    }
// si la matriz es simetrica
    public static void matrizSimetrica(int [][] matriz, int f, int c) {
        int aux;
        for (int i =0; i <f; i ++)
        {
            for (int j =0; j < i;j ++)
            {
                aux = matriz[i][j];
                matriz [i][j] = matriz [j][i];
                matriz [j][i]= aux;
            }
        }
    }
public static int [][] matrizAsimetrica(int [][] matriz) {
    int[][] matrizTranspuesta = new int [matriz[0].length][matriz.length];
        for (int i =0; i < matriz.length; i ++)
        {
            for(int j =0; j < matriz[i].length; j ++)
            {       
                   matrizTranspuesta[j][i] = matriz[i][j];
            }
        }
    return matrizTranspuesta;
}
// pintar matriz
    public static void mostrarMatriz(int matriz[][]) {  
        for (int i =0; i <matriz.length; i ++)
        {
            for (int j = 0; j < matriz[0].length; j++)
            {
                System.out.print("["+matriz[i][j]+"]");
            }
            System.out.println();
        }  
    }
    
}
